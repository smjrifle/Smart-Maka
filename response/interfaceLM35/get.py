import serial
import sys
repeatSend = 10
test=serial.Serial("/dev/ttyUSB0",9600,timeout=3)
if test < 0:
    sys.exit( "Serial port cannot be accessed")

test.open()
mode = 'none'
temperature = ""
while 1:
    data=test.read()
    #print data
    if data=='T':
        mode = 'temp'
    elif data == '#':
        print "Current temperature is " + temperature + "C"
        mode = ''
        break
      #  temperature = ""
    elif mode == 'temp':
        temperature += str(data)
