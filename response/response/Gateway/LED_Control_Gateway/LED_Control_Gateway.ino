

#include <RFM12B.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(5, 6); // RX, TX

#define NETWORKID       212
#define NODEID            1        // network ID used for this unit
#define KEY "ABCDABCDABCDABCD"     ///
#define HUBNUMBER 12345
#define EOL '#'

#define REQUESTACK     true        // whether to request ACKs for sent messages
#define ACK_TIME         50        // # of ms to wait for an ack
#define REGINTDELAY      50  

#define TXWAIT         100
#define ARD2MON_SERIAL_BAUD  9600   //The baud rate between arduino and serial monitor 
#define ARD2PI_SERIAL_BAUD  4800    // The baud rate between Arduino to raspberrypi

#define LED               9       // Status of RF tx/rx
#define REGINT            4
char  input;

RFM12B radio;
char mode = 'D';      

char pinState = '0';

char nodeId = 2;      //node to send message to
char senderNodeId;  //The node id of the Sender

char recvBuf[7];    // RF recieve buffer
char sendBuf[7];  // RF send buffer
char serialBuf[7];

void initRadio(){
  Serial.begin(ARD2MON_SERIAL_BAUD);
  mySerial.begin(ARD2PI_SERIAL_BAUD);
  
  Serial.print("Initializing Radio..");
  radio.Initialize(NODEID, RF12_433MHZ, NETWORKID);
  radio.Encrypt((uint8_t*)KEY);
  Serial.println("Success");
 
  Serial.println("[System ready]");
}
void setup()
{
  pinMode(LED, OUTPUT);
  pinMode(REGINT,OUTPUT);
  initRadio();

}

void loop()
{
  boolean registering=false;
if (mySerial.available() > 0) {
    input = mySerial.read();
    Serial.write(input);
    if (input == 'w')  // Write mode
    {
      mode = 'w';
    }
    else if(input == 'R'){
     mode = 'R';
    }
   
    else if (input == 'r'){   // Read mode 
       Serial.print("Pin Mode : Read");
      mode = 'r';
    }
    
    else if(input == '1' || input == '0'|| input == '-1')  
     pinState = input;
     
    else if (input >= '2' && input <= '9') //[2 to 9]
    {
      nodeId = input-48;
      Serial.print("Switched to node [" );
      Serial.print(nodeId);
      Serial.println("]...\n");
    }
    else if (input == '#' && mode !='D') {
      sendMsg();
    }
    else
    {
      Serial.println("Invalid input, try again: \n"+input);   
    }
  }
  
  recvMsg();
  if(recvBuf[0]=='R'){        // A module is requesting for Registration
  sprintf(serialBuf, "%cd%cn%d%c", recvBuf[0],recvBuf[1],senderNodeId,EOL); // [R:N:I] (R is register code, N is the device name, I is the node id)
  mySerial.flush();
  mySerial.print(serialBuf);
  recvBuf[0]=' '; 
  registering=true;
  digitalWrite(REGINT,HIGH);
  delay(REGINTDELAY); 
  digitalWrite(REGINT,LOW);

}
  
  
  
}

void sendMsg()
{
  char success = 0;
  long now = millis();
  sprintf(sendBuf, "%c:%c", mode, pinState);
  while(((millis()-now)<TXWAIT) && success==0){
  radio.Send(nodeId, sendBuf, 3, REQUESTACK);
  Serial.print("Request sent to node : ");
  Serial.println(nodeId);
  if(waitForAck()){
    mySerial.println('1');
    Serial.println("ACK ok");
    success=1;
  }
  else{
    mySerial.println('0');
    Serial.println("NO ACk reply");
  }
    }
  //mySerial.flush();
  Blink(LED, 5);
}


static bool waitForAck() {
  long now = millis();
  while (millis() - now <= ACK_TIME)
    if (radio.ACKReceived(nodeId))
      return true;
  return false;
}

void Blink(byte PIN, byte DELAY_MS)
{
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN,HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN,LOW);
}

void recvMsg(void){
if (radio.ReceiveComplete())
  {
    
    if (radio.CRCPass())
     {
       byte i;
      senderNodeId= radio.GetSender();
      Serial.print('[');Serial.print(radio.GetSender());Serial.print("] ");
      Serial.print("Recieved: ");
      for (i = 0; i < *radio.DataLen; i++){ 
        Serial.print((char)radio.Data[i]);
        recvBuf[i]=radio.Data[i];
      }
      recvBuf[i] = '\0';
      if (radio.ACKRequested())
      {
        radio.SendACK();
        Serial.print(" - ACK sent");
      }
    }
    else
    {
      Serial.print("BAD-CRC");

    }
    
    Serial.println();
  }
  else 
    return;
}
