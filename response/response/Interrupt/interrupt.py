import logging
import serial
import sys
from time import sleep
import RPIO

repeatSend = 10
interruptFlag = False
test=serial.Serial("/dev/ttyAMA0",4800)
if test <0:
    sys.exit( "Serial port cannot be accessed")

test.open()
#print test  #uncomment this if you want to see the status of the port
log_format = '%(levelname)s | %(asctime)-15s | %(message)s'
logging.basicConfig(format=log_format, level=logging.DEBUG)

def parseData(gpio_id, value):
    data = ' '
    logging.info("New value for GPIO %s: %s" % (gpio_id, value))
    while data!='#':
        data=test.read()
        print data
    interruptFlag=True

RPIO.add_interrupt_callback(23, parseData, edge='rising')

RPIO.wait_for_interrupts(threaded=True)
while interruptFlag==False:
    print "Waiting..\n"
    sleep(1)

test.close()
