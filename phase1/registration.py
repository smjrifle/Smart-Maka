import serial
import sys
from time import sleep
from urllib2 import Request, urlopen, URLError
mySerial = serial.Serial("/dev/ttyAMA0",4800,timeout=3)
if(mySerial < 0):
    sys.exit( "Serial port cannot be accessed")

mySerial.open()
count = 0
nodeid = ""
device = ""
prevCmd = ""
while True:
    data = mySerial.read()
    if(data == '#'):
        print "Device " + device + " node " + nodeid
        request = Request('http://localhost/registration.php?nodeid=' + str(nodeid) + "&device=" + str(device))
        try:
            response = urlopen(request)
            readResponse = response.read()
            response = readResponse[1:200]
            sleep(0.5)
            mySerial.write('R' + response + "#")
            print 'R' + response + "#"
            mySerial.close()
        except URLError, e:
            print 'No Response :(. Got an error code:', e
        break
    elif(not data.isdigit()):
        prevCmd = data
    elif(prevCmd == 'd'):
        device += str(data)
    elif(prevCmd == 'n'):
        nodeid += str(data)
#mySerial.close()



