import serial
import sys
from time import sleep

repeatSend = 10
test=serial.Serial("/dev/ttyAMA0",4800,timeout=3)
if test <0:
    sys.exit( "Serial port cannot be accessed")

test.open()

#print test  #uncomment this if you want to see the status of the port


#print "To send -> "+ sys.argv[1] + " " +  sys.argv[2] #uncomment this if you want to see the argument sent
count = 0

while count<repeatSend:

    test.write(sys.argv[1] +  sys.argv[2]+'#')
    data = test.read()
    if data == '1':
        print "Transaction successful"
        break
    else:
        count = count + 1
        print "Sending...\n"
        if count == repeatSend:
            print "Cannot connect. Please check if the module is powered."
test.close()



