import logging
import serial
import sys
from time import sleep
import RPIO
import subprocess
import time

repeatSend = 10
interruptFlag = False
mySerial = serial.Serial("/dev/ttyAMA0",4800)
if(mySerial < 0):
    sys.exit("Serial port cannot be accessed")
mySerial.open()
#print mySerial  #uncomment this if you want to see the status of the port
#log_format = '%(levelname)s | %(asctime)-15s | %(message)s'
#logging.basicConfig(format=log_format, level=logging.DEBUG)
def parseData(gpio_id, value):
    f = open('interrupt.log', 'a')
    data = ' '
    #logging.info("New value for GPIO %s: %s" % (gpio_id, value))
    data=mySerial.read()
    mySerial.write(data)
    interruptFlag = True
    if(data == 'R'):
        f.write("Registration Mode Opened " + time.strftime("%c") + "\n")
        respin = subprocess.Popen("python registration.py", stdout=subprocess.PIPE, stderr=None, shell=True)
        result = respin.communicate()[0]
        f.write(result)
        f.write("Registration Closed at " + time.strftime("%c") + "\n")
        interruptFlag = False
        mySerial.flushInput()
        mySerial.flushOutput()
        f.close()
try:
    RPIO.add_interrupt_callback(23, parseData, edge = 'rising')
    RPIO.wait_for_interrupts(threaded = True)
    while(interruptFlag == False):
        sleep(1)
except KeyboardInterrupt:
    mySerial.close()
    RPIO.cleanup()
#mySerial.close()
