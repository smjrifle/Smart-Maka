#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <wiringPi.h>
#include <wiringSerial.h>

int main (int argc, char *argv[])
{
  char fd ;
  int count ;
  unsigned int nextTime ;
  if ((fd = serialOpen ("/dev/ttyAMA0", 9600)) < 0)
  {
    fprintf (stderr, "Unable to open serial device: %s\n", strerror (errno)) ;
    return 1 ;
  }

  if (wiringPiSetup () == -1)
  {
    fprintf (stdout, "Unable to start wiringPi: %s\n", strerror (errno)) ;
    return 1 ;
  }

     char *c, *d;
     int *value;
     c = argv[1];
     d = argv[2];
//     printf("%c %c",*c,*d);
//   value = atoi(argv[1]);
     serialPutchar (fd, *c);
     serialPutchar (fd, *d);
     serialPutchar (fd, '\n');
     printf("%d",serialDataAvail(fd));
//   serialPuts(fd,c);
while (serialDataAvail (fd))
    {
      printf (" -> %3d", serialGetchar (fd)) ;
      //fflush (stdout) ;
    }
     return 0 ;
}
