
#include <RFM12B.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(5, 6); // RX, TX

#define NETWORKID       212
#define NODEID            1        // network ID used for this unit
#define KEY "ABCDABCDABCDABCD"     //(16 bytes of your choice - keep the same on all encrypted nodes)
#define REQUESTACK     true        // whether to request ACKs for sent messages
#define ACK_TIME         50        // # of ms to wait for an ack
#define ARD2MON_SERIAL_BAUD  9600   //The baud rate between arduino and serial monitor 
#define ARD2PI_SERIAL_BAUD  4800    // The baud rate between Arduino to raspberrypi


#define LED               9       // Status of RF tx/rx

uint8_t input;
RFM12B radio;
char mode = 'D';      

char pinState = '0';

byte nodeId = 2;      //node to send message to
char *recvBuf = "";    // RF recieve buffer
char * sendBuf="D:W";  // RF send buffer
char serialBuf[5];

char senderNodeId;  //The node id of the Sender





void setup()
{
  pinMode(LED, OUTPUT);
  Serial.begin(ARD2MON_SERIAL_BAUD);
  mySerial.begin(ARD2PI_SERIAL_BAUD);
  
  Serial.print("Initializing Radio..");
  radio.Initialize(NODEID, RF12_433MHZ, NETWORKID);
  radio.Encrypt((uint8_t*)KEY);
  Serial.println("Success");
 
  Serial.println("[System ready]");
}

void loop()
{
  //check for serial input (from 2-9, to correspond to node IDs [2,9])
  if (mySerial.available() > 0) {
    input = mySerial.read();
    Serial.write(input);
    
    if (input == 'w')  // Write mode
    {
      Serial.print("Pin Mode : Write ");
      mode = 'w';
    }
    else if (input == 'r'){   // Read mode 
       Serial.print("Pin Mode : Read");
      mode = 'r';
    }
    
    else if(input == '1' || input == '0')  
     pinState = input;
     
    else if (input >= '2' && input <= '9') //[2 to 9]
    {
      nodeId = input-48;
      Serial.print("Switched to node [" );
      Serial.print(nodeId);
      Serial.println("]...\n");
    }
    else if (input == '#') {
    
      sendMessage();
    }
    else
    {
      Serial.println("Invalid input, try again: \n"+input);
      
    }
  }
  
  recieveMessage();
  
  if(recvBuf[0]=='R'){  // A module is requesting for Registration
  sprintf(serialBuf, "%cn%c#", recvBuf[0], senderNodeId);
  mySerial.print(serialBuf);
  recvBuf[0]=' '; 
}
  
  
  
}

void sendMessage()
{
  sprintf(sendBuf, "%c:%c", mode, pinState);
  radio.Send(nodeId, sendBuf, 3, REQUESTACK);
  Serial.print("Request sent to node : ");
  Serial.println(nodeId);
  if(waitForAck()){
    mySerial.println('1');
    Serial.println("ACK ok");
  }
  else{
    mySerial.println('0');
    Serial.println("NO ACk reply");
  }
  
  //mySerial.flush();
  Blink(LED, 5);
}

// wait up to ACK_TIME for proper ACK, return true if received
static bool waitForAck() {
  long now = millis();
  while (millis() - now <= ACK_TIME)
    if (radio.ACKReceived(nodeId))
      return true;
  return false;
}

void Blink(byte PIN, byte DELAY_MS)
{
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN,HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN,LOW);
}

void recieveMessage(void){
if (radio.ReceiveComplete())
  {
    
    if (radio.CRCPass())
     {
       byte i;
      senderNodeId= radio.GetSender();
      Serial.print('[');Serial.print(radio.GetSender());Serial.print("] ");
      Serial.print("Recieved: ");
      for (i = 0; i < *radio.DataLen; i++){ //can also use radio.GetDataLen() if you don't like pointers
        Serial.print((char)radio.Data[i]);
        recvBuf[i]=radio.Data[i];
      }
      recvBuf[i] = '\n';
      if (radio.ACKRequested())
      {
        radio.SendACK();
        Serial.print(" - ACK sent");
      }
    }
    else
    {
      Serial.print("BAD-CRC");

    }
    
    Serial.println();
  }
  else 
    return;
}
